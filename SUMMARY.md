# Summary

* [Introduction](README.md)
* [Getting Started](gettingstarted.md)
* [Using the Repositories](using.md)
* [Projects](projects.md)

## Best Practices
* [Sourcetype Documentation](styleguide/repository_structure.md)
* [Git Repository Structure](styleguide/repository_structure.md)










