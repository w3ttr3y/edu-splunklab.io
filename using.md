# Dipping Your Toes
Your best option may be to just download an archive of files if you:
1. aren't familiar with git
2. are unsure about using git to version control your Splunk configuration
3. want to use a very simple, non-contributing git repository

If you want to fully embrace git and follow best practices, please see [Jumping In](#jumping-in)

## Small Number of TA's
See the list of TA's.


## Small Number of TA's Leveraging Version Control

## Jumping In

## Deployment Server

## Master Node

## Git Directly



