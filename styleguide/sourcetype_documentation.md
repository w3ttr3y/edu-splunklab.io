
Each project that has a TA needs to have a SOURCETYPES.md files in the root directory.

In a heading 2, it should list the name of the project
In an html definition list, it should list the sourcetypes in dt tags with a description of the sourcetype in dd tags.

For example:
```
# Shibboleth

<dl>
  <dt>shibboleth</dt>
  <dd>TODO: write a definition for shibboleth sourcetype</dd>

  <dt>shibboleth_web</dt>
  <dd>TODO: write a definition for shibboleth sourcetype</dd>

  <dt>shibboleth_sp_shibd</dt>
  <dd>TODO: write a definition for shibboleth sourcetype</dd>

  <dt>shibboleth_sp_shibd:warn</dt>
  <dd>TODO: write a definition for shibboleth sourcetype</dd>

  <dt>shibboleth_sp_transaction</dt>
  <dd>TODO: write a definition for shibboleth sourcetype</dd>
</dl>
```


