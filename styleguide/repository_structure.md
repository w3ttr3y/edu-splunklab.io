# Branches

Branches in the Splunk Edu name space typically are a subset of:

<dl>
  <dt>TA</dt>
  <dd>The files that are required to go on search heads, indexers, and heavy forwarders</dd>
  <dt>IA</dt>
  <dd>TODO</dd>
  <dt>SC?</dt>
  <dd>Do we include an SC?</dd>
  <dt>AP?</dt>
  <dd>Do we include an AP?</dd>
</dl>

In certain circumstances, it can be advantagous to have multiple add-on's for a given type.  For example, shibboleth has both IDP's and SP's so having IA-idp and IA-sp is acceptable.

# Branch Usage
Please note that while branches are simply named after the type, it is assumed they will be extracted into directories of the form `<branch root>-<project>-<branch suffix>`

For example for shibboleth, you would likely have:
<dl>
  <dt>TA-shibboleth</dt>
  <dd>The files from the TA branch of the shibboleth project</dd>
  <dd>Files from the shibboleth project that need to go on search heads, indexers, and heavy forwarders</dd>
  <dt>IA-shibboleth-idp</dt>
  <dd>The files from the IA-idp branch of the shibboleth project</dd>
  <dd>Files necessary to read the logs from an idp server</dd>
  <dt>IA-shibboleth-sp</dt>
  <dd>The files from the IA-sp branch of the shibboleth project</dd>
  <dd>Files necessary to read the logs from an sp server</dd>
</dl>

