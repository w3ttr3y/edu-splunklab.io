<!--- Please list projects in alphabetical order.  
Please also include a SOURCETYPES.md reference that is a definition list of all the sourcetypes in the Add-on
Note: as of 4/19/2017 when linking, you need to use git+https... format; using git:// doesn't work -->
Shibboleth
{% include "git+https://w3ttr3y@gitlab.com/edu-splunk/shibboleth.git/SOURCETYPES.md" %}
